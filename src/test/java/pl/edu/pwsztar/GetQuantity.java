package pl.edu.pwsztar;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class GetQuantity {
    @Test
    void getQuantityByName() {
        ShoppingCart shoppingCart = new ShoppingCart();
        Product mock1 = new Product("Woda",3,3);
        Product mock2 = new Product("Woda",3,3);
        Product mock3 = new Product("Mleko",4,5);
        Product mock4 = new Product("Sok",25,1);
        shoppingCart.addProducts(mock1.getName(), mock1.getPrice(), mock1.getQuantity());

        assertEquals(3, shoppingCart.getQuantityOfProduct(mock1.getName()));
        shoppingCart.addProducts(mock2.getName(), mock2.getPrice(), mock2.getQuantity());
        shoppingCart.addProducts(mock3.getName(), mock3.getPrice(), mock3.getQuantity());
        shoppingCart.addProducts(mock4.getName(), mock4.getPrice(), mock4.getQuantity());
        assertEquals(6, shoppingCart.getQuantityOfProduct(mock2.getName()));
        assertEquals(5, shoppingCart.getQuantityOfProduct(mock3.getName()));
        assertEquals(1, shoppingCart.getQuantityOfProduct(mock4.getName()));
        shoppingCart.deleteProducts(mock4.getName(),1);
        assertEquals(0, shoppingCart.getQuantityOfProduct(mock4.getName()));
    }
}
