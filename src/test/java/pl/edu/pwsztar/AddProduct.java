package pl.edu.pwsztar;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class AddProduct {
    ShoppingCart shoppingCart = new ShoppingCart();
    @Test
    void test() {
        Product mock1 = new Product("Sok",3,100);
        Product mock2 = new Product("Szynka",4,100);
        Product mock3 = new Product("Pomidory",5,100);


        assertTrue(shoppingCart.addProducts(mock1.getName(), mock1.getPrice(), mock1.getQuantity()));
        assertTrue(shoppingCart.addProducts(mock2.getName(), mock2.getPrice(), mock2.getQuantity()));
        assertTrue(shoppingCart.addProducts(mock3.getName(), mock3.getPrice(), mock3.getQuantity()));

    }
}
