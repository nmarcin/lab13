package pl.edu.pwsztar;

public class Product {
    private String name;
    private int price;
    private int quantity;

    public Product(String name, int price, int quantity) {
        this.name = name;
        this.price = price;
        this.quantity = quantity;
    }

    public String getName() {
        return name;
    }

    public int getPrice() {
        return price;
    }

    public int getQuantity() {
        return quantity;
    }

    int addQuantity(int quanti){
        quantity += quanti;
        return quantity;
    }
    int delete(int quanti){
        quantity -= quanti;
        return quantity;
    }
}
